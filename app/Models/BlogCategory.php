<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class BlogCategory
 * @package App\Models
 *
 * @property int $id
 * @property int $parent_id
 * @property string $slug
 * @property string $title
 * @property string|null $description
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property-read BlogCategory $parentCategory
 * @property-read string $parentTitle
 */
class BlogCategory extends Model
{
    use SoftDeletes;
    /**
     * root id
     */
    const ROOT = 1;
    protected $fillable = [
        'title',
        'slug',
        'parent_id',
        'description',
    ];

    public function parentCategory()
    {
        return $this->belongsTo(BlogCategory::class, 'parent_id', 'id');
    }

    public function getParentTitleAttribute()
    {
        $title = $this->parentCategory->title
            ?? (
                $this->isRoot()
                ? 'Корень'
                : '???'
            );
        return $title;
    }

    private function isRoot()
    {
        return $this->id === BlogCategory::ROOT;
    }

//    public function getTitleAttribute($fromDb)
//    {
//        return mb_strtoupper($fromDb);
//    }

//    public function setTitleAttribute($incoming)
//    {
//        return mb_strtolower($incoming);
//    }

}
