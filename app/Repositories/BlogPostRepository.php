<?php

namespace App\Repositories;

use App\Models\BlogPost as Model;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class BlogPostRepository
 * @package App\Repositories
 */
class BlogPostRepository extends CoreRepository
{

    /**
     * Получить модель для редактирования в админке.
     *
     * @param int $id
     */
    public function getEdit($id)
    {
        return $this->startConditions()->find($id);
    }

    /**
     * получить список категорий для вывода в выпадающем списке.
     *
     * @return Collection|static[]
     */
    public function getForComboBox()
    {
        $columns = implode(', ', [
           'id',
            'CONCAT (id, ". ", title) AS id_title',
        ]);

        $result = $this->startConditions()
            ->selectRaw($columns)
            ->toBase()
            ->get();

        return $result;
    }

    /**
     * Получить категории для вывода пагинатором.
     *
     * @return LengthAwarePaginator
     */
    public function getAllWithPaginate()
    {
        $columns = [
            'id',
            'title',
            'slug',
            'is_published',
            'published_at',
            'user_id',
            'category_id',
        ];

        $result = $this->startConditions()
            ->select($columns)
            ->orderBy('id', 'DESC')
            ->with('category:id,title', 'user:id,name')
            ->paginate(25);

        return $result;
    }

    protected function getModelClass()
    {
        return Model::class;
    }
}
